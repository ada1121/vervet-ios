//
//  FilterMenuVC.swift
//  Vervet
//
//  Created by Ubuntu on 11/18/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class FilterMenuVC: BaseVC2 {

    @IBOutlet weak var spaceView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    @IBAction func gotoback(_ sender: Any) {
        closeMenu()
    }
    @IBAction func spaceClicked(_ sender: Any) {
        closeMenu()
    }
    
    @IBAction func gasAClicked(_ sender: Any) {
        gasA = false
        gasB = true
        gasC = true
        closeMenu()
//        let tovc = self.storyboard?.instantiateViewController(identifier: "StatsVC")
//        tovc?.modalPresentationStyle = .fullScreen
//        self.present(tovc!, animated: false, completion: nil)
    }
    @IBAction func gasBClicked(_ sender: Any) {
        gasA = true
        gasB = false
        gasC = true
        closeMenu()
//        let tovc = self.storyboard?.instantiateViewController(identifier: "StatsVC")
//        tovc?.modalPresentationStyle = .fullScreen
//        self.present(tovc!, animated: false, completion: nil)
    }
    @IBAction func gasCClicked(_ sender: Any) {
        gasA = true
        gasB = true
        gasC = false
        closeMenu()
//        let tovc = self.storyboard?.instantiateViewController(identifier: "StatsVC")
//        tovc?.modalPresentationStyle = .fullScreen
//        self.present(tovc!, animated: false, completion: nil)
    }
}
