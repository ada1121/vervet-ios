//
//  LoginCV.swift
//  Vervet
//
//  Created by Admin on 11/10/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit
import GoogleSignIn
import FirebaseCore
import FBSDKLoginKit
import Foundation

import SwiftValidator
import IQKeyboardManagerSwift
import SwiftyJSON
import SwiftyUserDefaults
import GDCheckbox


class LoginVC: BaseVC1 ,ValidationDelegate, UITextFieldDelegate,UINavigationControllerDelegate, UIImagePickerControllerDelegate{

    @IBOutlet weak var edtEmail: UITextField!
    @IBOutlet weak var edtPwd: UITextField!
    
    let validator = Validator()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        GIDSignIn.sharedInstance()?.delegate = self
        // Do any additional setup after loading the view.
        navBarHidden()
        self.hideKeyboardWhenTappedAround()
    }
    @IBAction func gotoSignUp(_ sender: Any) {
        gotoNavPresent1("SignUpVC")
    }
    @IBAction func loginBtnClicked(_ sender: Any) {
        
        validator.registerField(edtEmail, errorLabel: nil , rules: [RequiredRule(),EmailRule()])
        validator.registerField(edtPwd, errorLabel: nil , rules: [RequiredRule(),AlphaNumericRule()])
       
        validator.styleTransformers(success:{ (validationRule) -> Void in
            
            // clear error label
            validationRule.errorLabel?.isHidden = true
            validationRule.errorLabel?.text = ""
            
            if let textField = validationRule.field as? UITextField {
                textField.layer.borderColor = UIColor.green.cgColor
                textField.layer.borderWidth = 1
            } else if let textField = validationRule.field as? UITextView {
                textField.layer.borderColor = UIColor.green.cgColor
                textField.layer.borderWidth = 1
            }
        }, error:{ (validationError) -> Void in
            print("error")
            validationError.errorLabel?.isHidden = false
            validationError.errorLabel?.text = validationError.errorMessage
            if let textField = validationError.field as? UITextField {
                textField.layer.borderColor = UIColor.red.cgColor
                textField.layer.borderWidth = 1.0
            } else if let textField = validationError.field as? UITextView {
                textField.layer.borderColor = UIColor.red.cgColor
                textField.layer.borderWidth = 1.0
            }
        })
        validator.validate(self)
        
    }
    
    func validationSuccessful() {
        self.validationSuccess()
       }
       
       func validationFailed(_ errors: [(Validatable, ValidationError)]) {
           print("validation error")
       }
    
    func validationSuccess() {
        
        let loginManager = FirebaseAuthManager()
        guard let email = edtEmail.text, let password = edtPwd.text else { return }
        loginManager.signIn(email: email, pass: password) {[weak self] (success) in
            guard let `self` = self else { return }
            var message: String = ""
            if (success) {
                message = "User was sucessfully logged in."
                
                self.loginSuccess()
            } else {
                message = "User Not Exist or Wrong Password!"
            }
            let alertController = UIAlertController(title: nil, message: message, preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
            self.display(alertController: alertController)
        }

    }
    
    func display(alertController: UIAlertController) {
        self.present(alertController, animated: true, completion: nil)
    }
    
    func loginSuccess() {
        
       AppDelegate.setupReferenceUITabBarController()
        AppDelegate.setupHHTabBarView()
        
        mainNav = NavigationController(rootViewController: AppDelegate.referenceUITabBarController)
        
        mainNav.modalPresentationStyle = .fullScreen
        self.present(mainNav, animated: true, completion: nil)
        
        //mainNav.setNavigationBarHidden(true, animated: true)
        mainNav.isNavigationBarHidden = true
        mainNav.navigationBar.setBackgroundImage(UIImage(), for: .default)
        mainNav.navigationBar.shadowImage = UIImage()
        mainNav.navigationBar.isTranslucent = true
        mainNav.view.backgroundColor = .clear
    }
    
    @IBAction func onGoogleSign(_ sender: Any) {
    
        GIDSignIn.sharedInstance()?.presentingViewController = self
        GIDSignIn.sharedInstance()?.signIn()
    }
    
    
    @IBAction func onFacebookSign(_ sender: Any) {
        
        let loginManager = LoginManager()
        
        loginManager.logIn(permissions: ["public_profile", "email"], from: self) { [weak self](result, error) in
            
            if error != nil {
                print("Login failed")
            } else {
                if result!.isCancelled { print("login canceled") }
                else { self!.getFaceBookUserData()
                       
                }
            }
        }
    
    
    }

    func getFaceBookUserData() {
            
            let connection = GraphRequestConnection()
            connection.add(GraphRequest(graphPath: "/me", parameters: ["fields": "id, name, email, first_name, last_name"])) { (httpResponse, result, error) in

                print("graph AP I result: ", result as Any)
//                let jsonResult = JSON(result)
//                let fname = jsonResult["first_name"].stringValue
//                let lname = jsonResult["last_name"].stringValue
//                let email = jsonResult["email"].stringValue
//
//                let id = jsonResult["id"].intValue
//                let profile_photo = "http://graph.facebook.com/\(id)/picture?type=large"
//
//
//                print(id, " : ", email)
//                print(fname, " : ", lname)
//                print(profile_photo)                
                
            }

            connection.start()
        }
}

extension LoginVC: GIDSignInDelegate {
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
            
        if let error = error {
            print(error.localizedDescription)
            return
        }
//        guard  let auth = user.authentication else {
//            return
//        }
        
        // Perform any operations on signed in user here.
        //let userId = user.userID                  // For client-side use only!
        //let idToken = user.authentication.idToken // Safe to send to the server
        //let fullName = user.profile.name
        //let givenName = user.profile.givenName
        //let familyName = user.profile.familyName
        let email : String = user.profile.email!
        
        self.loginSuccess()
        print(email)
            
//        gotoSigninAPI(email : email, pwd : "", auth_type : Constants.mailType[2])
        
//        let credentials = GoogleAuthProvider.credential(withIDToken: auth.idToken, accessToken: auth.accessToken)
    
//        Auth.auth().signIn(with: credentials) { (authResult, error) in
//
//            if let error = error {
//                print(error.localizedDescription)
//                return
//            }
//
//            // TODO: ----
//        }
        
    }
        

}
