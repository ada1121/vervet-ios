//
//  LoadingVC.swift
//  Vervet
//
//  Created by Admin on 11/11/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class LoadingVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        let jeremyGif = UIImage.gifImageWithName("tenor")
        let imageView = UIImageView(image: jeremyGif)
        imageView.frame = CGRect(x: 20.0, y: self.view.frame.size.height/2.3, width: self.view.frame.size.width - 40, height: 120.0)
        view.addSubview(imageView)
        
        
        
        AppDelegate.setupReferenceUITabBarController()
        AppDelegate.setupHHTabBarView()
        
        
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 5.0, execute: {
            mainNav = NavigationController(rootViewController: AppDelegate.referenceUITabBarController)
            
            mainNav.modalPresentationStyle = .fullScreen
            self.present(mainNav, animated: true, completion: nil)
            
            //mainNav.setNavigationBarHidden(true, animated: true)
            mainNav.isNavigationBarHidden = true
            mainNav.navigationBar.setBackgroundImage(UIImage(), for: .default)
            mainNav.navigationBar.shadowImage = UIImage()
            mainNav.navigationBar.isTranslucent = true
            mainNav.view.backgroundColor = .clear
                    
            }
        )
    }
    
    

}
