//
//  MenuVC.swift
//  Vervet
//
//  Created by Ubuntu on 11/12/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class MenuVC: BaseVC {

    @IBOutlet weak var spaceView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        spaceView.addTapGesture(tapNumber: 1, target: self, action: #selector(onSpace))

        // Do any additional setup after loading the view.
    }
    
    @objc func onSpace(gesture: UITapGestureRecognizer) -> Void {
           closeMenu()
           openStatue = false
           exceptionStatue = false
           
       }
    @IBAction func profileBtnclicked(_ sender: Any) {
        gotoNavPresent("ProfileVC")
        closeMenu()
    }
    @IBAction func stateBtnclicked(_ sender: Any) {
        gotoNavPresent("StatsVC")
        closeMenu()
    }
    @IBAction func settingBtnclicked(_ sender: Any) {
        gotoNavPresent("SettingsVC")
        closeMenu()
    }
    @IBAction func helpBtnclicked(_ sender: Any) {
        print("No helps")
        closeMenu()
    }
    @IBAction func logoutBtnclicked(_ sender: Any) {
        
        let loginVC = self.storyboard?.instantiateViewController( withIdentifier: "LoginVC")
        
      loginNav = UINavigationController(rootViewController: loginVC!)
        
      loginNav.modalPresentationStyle = .fullScreen
       self.present(loginNav,animated: true,completion: nil)
       closeMenu()
    }
    
}
