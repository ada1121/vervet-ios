//
//  AddDeviceVC.swift
//  Vervet
//
//  Created by Admin on 11/11/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class AddDeviceVC: BaseVC1 {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    @IBAction func backBtnClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func gotoAddDevice(_ sender: Any) {
        gotoNavPresent1("WifiVC")
    }
    
}
