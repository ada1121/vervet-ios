//
//  SignUpVC.swift
//  Vervet
//
//  Created by Admin on 11/10/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit
import Foundation
import UIKit
import SwiftValidator
import IQKeyboardManagerSwift
import SwiftyJSON
import SwiftyUserDefaults
import GDCheckbox

class SignUpVC : BaseVC1,ValidationDelegate, UITextFieldDelegate,UINavigationControllerDelegate, UIImagePickerControllerDelegate {

    @IBOutlet weak var edt_Email: UITextField!
    @IBOutlet weak var edt_Pwd: UITextField!
    @IBOutlet weak var edt_confirmPwd: UITextField!
    
    @IBOutlet weak var edt_fullName: UITextField!
    @IBOutlet weak var edt_age: UITextField!
    @IBOutlet weak var edt_currentLocation: UITextField!
    @IBOutlet weak var edt_contactperson: UITextField!
    @IBOutlet weak var edt_contact: UITextField!
    
    
    let validator = Validator()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.hideKeyboardWhenTappedAround()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
    }
    
    func display(alertController: UIAlertController) {
        self.present(alertController, animated: true, completion: nil)
    }
    
    @IBAction func backBtnClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func registerBtnClicked(_ sender: Any) {
      
        validator.registerField(edt_fullName, errorLabel: nil , rules: [RequiredRule()])
        validator.registerField(edt_age, errorLabel: nil , rules: [RequiredRule()])
        validator.registerField(edt_currentLocation, errorLabel: nil , rules: [RequiredRule()])
        validator.registerField(edt_contactperson, errorLabel: nil , rules: [RequiredRule()])
        validator.registerField(edt_contact, errorLabel: nil , rules: [RequiredRule()])
        
        validator.registerField(edt_Email, errorLabel: nil , rules: [RequiredRule(),EmailRule()])
        validator.registerField(edt_Pwd, errorLabel: nil , rules: [RequiredRule(),AlphaNumericRule(),MinLengthRule(length: 6)])
        validator.registerField(edt_confirmPwd, errorLabel: nil , rules: [RequiredRule(),ConfirmationRule(confirmField: edt_Pwd)])
        validator.styleTransformers(success:{ (validationRule) -> Void in
            
            // clear error label
            validationRule.errorLabel?.isHidden = true
            validationRule.errorLabel?.text = ""
            
            if let textField = validationRule.field as? UITextField {
                textField.layer.borderColor = UIColor.green.cgColor
                textField.layer.borderWidth = 1
            } else if let textField = validationRule.field as? UITextView {
                textField.layer.borderColor = UIColor.green.cgColor
                textField.layer.borderWidth = 1
            }
        }, error:{ (validationError) -> Void in
            print("error")
            validationError.errorLabel?.isHidden = false
            validationError.errorLabel?.text = validationError.errorMessage
            if let textField = validationError.field as? UITextField {
                textField.layer.borderColor = UIColor.red.cgColor
                textField.layer.borderWidth = 1.0
            } else if let textField = validationError.field as? UITextView {
                textField.layer.borderColor = UIColor.red.cgColor
                textField.layer.borderWidth = 1.0
            }
        })
        validator.validate(self)
    }
    
    func validationSuccessful() {
        
      gotoFireSignUp()
            
    }
    
    func validationFailed(_ errors: [(Validatable, ValidationError)]) {
        print("validation error")
        
        let alertController = UIAlertController(title: nil, message: "Please Check All !     Password Must Be more than 6 Characters!", preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        self.display(alertController: alertController)
        
    }
    func gotoFireSignUp()  {
        let signUpManager = FirebaseAuthManager()
        if let email = edt_Email.text, let password = edt_Pwd.text {
            signUpManager.createUser(email: email, password: password) {[weak self] (success) in
                guard let `self` = self else { return }
                var message: String = ""
                if (success) {
                    message = "User was sucessfully created."
                } else {
                    message = "There was an error."
                }
                let alertController = UIAlertController(title: nil, message: message, preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                self.display(alertController: alertController)
            }
        }

    }
    
}
