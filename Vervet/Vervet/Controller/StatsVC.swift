//
//  StatsVC.swift
//  Vervet
//
//  Created by Admin on 11/11/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class StatsVC: BaseVC2 {

    @IBOutlet weak var ImvgasC: UIImageView!
    @IBOutlet weak var ImvgasA: UIImageView!
    @IBOutlet weak var ImvgasB: UIImageView!
    
    @IBOutlet weak var UvcolorABtn: UIView!
    @IBOutlet weak var UvcolorBBtn: UIView!
    @IBOutlet weak var UvcolorCBtn: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        creatNav()
        colorChange()
    }
    @IBAction func backBtnClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func colorChange()  {
        if gasA {
            UvcolorABtn.backgroundColor = UIColor.init(named: "gasA")!.withAlphaComponent(0.2)
            ImvgasA.isHidden = true
            
        }
            
        if !gasA {
            UvcolorABtn.backgroundColor = UIColor.init(named: "gasA")!.withAlphaComponent(1)
            ImvgasA.isHidden = false
           
        }
        
        if gasB {
            UvcolorBBtn.backgroundColor = UIColor.init(named: "gasB")!.withAlphaComponent(0.2)
            ImvgasB.isHidden = true
            
        }
        
        if !gasB {
            UvcolorBBtn.backgroundColor = UIColor.init(named: "gasB")!.withAlphaComponent(1)
            ImvgasB.isHidden = false
            
        }
        
        if gasC {
            UvcolorCBtn.backgroundColor = UIColor.init(named: "gasC")!.withAlphaComponent(0.2)
            ImvgasC.isHidden = true
            
        }
        
        if !gasC {
            UvcolorCBtn.backgroundColor = UIColor.init(named: "gasC")!.withAlphaComponent(1)
            ImvgasC.isHidden = false
            
        }
    }
    @IBAction func gasAClicked(_ sender: Any) {
        gasA = !gasA
        colorChange()
    }
    @IBAction func gasBClicked(_ sender: Any) {
        gasB = !gasB
        colorChange()
    }
    @IBAction func gasCClicked(_ sender: Any) {
        
        gasC = !gasC
        colorChange()
    }
    @IBAction func filterBtnClicked(_ sender: Any) {
        openMenu()
    }
}
