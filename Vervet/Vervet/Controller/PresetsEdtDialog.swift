//
//  ViewController.swift
//  DPTimePicker
//
//  Created by Dario Pellegrini on 07/10/16.
//  Copyright © 2016 Dario Pellegrini. All rights reserved.
//

import UIKit

extension PresetsEdtDialog : DPTimePickerDelegate {
    
    func timePickerDidConfirm(_ hour: String, minute: String, timePicker: DPTimePicker) {
        let alert = UIAlertController(title: "Time selected", message: "\(hour) : \(minute)", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func timePickerDidClose(_ timePicker: DPTimePicker) {
        let alert = UIAlertController(title: "Closed", message: "Time picker closed", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}

class PresetsEdtDialog: UIViewController {
    
    @IBOutlet weak var topview: UIView!
    
    let timePicker: DPTimePicker = DPTimePicker.timePicker()
    let redColor = UIColor.red
    override func viewDidLoad() {
        super.viewDidLoad()
        UIApplication.shared.statusBarStyle = .lightContent
        
        timePicker.insertInView(topview)
        timePicker.delegate = self
        timePicker.closeButton.setTitleColor(redColor, for: .normal)
        timePicker.closeButton.setTitle("Close", for: .normal)
        timePicker.okButton.setTitleColor(redColor, for: .normal)
        timePicker.okButton.setTitle("OK", for: .normal)
        timePicker.backgroundColor = redColor
        timePicker.numbersColor = UIColor.white
        timePicker.linesColor = UIColor.white
        timePicker.pointsColor = UIColor.white
        timePicker.topGradientColor = redColor
        timePicker.bottomGradientColor = redColor
        timePicker.fadeAnimation = true
        timePicker.springAnimations = true
        timePicker.scrollAnimations = true
        timePicker.areLinesHidden = false
        timePicker.arePointsHidden = false
        timePicker.initialHour = "15"
        timePicker.initialMinute = "12"
        
        
        timePicker.show(nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
//    @IBAction func show(_ sender: UIButton) {
//        timePicker.show(nil)
//    }
}



