//
//  FireTabVC.swift
//  Vervet
//
//  Created by Ubuntu on 11/11/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class FireTabVC: BaseVC {
    
    var fire = false
    var motion = false
    var gas = false
    var index = 0
    
    @IBOutlet weak var settingView1: UIView!
    @IBOutlet weak var closedState1: UIView!
    @IBOutlet weak var settingView2: UIView!
    @IBOutlet weak var closedState2: UIView!
    @IBOutlet weak var settingView3: UIView!
    @IBOutlet weak var closedState3: UIView!
    
    @IBOutlet weak var fireBackGround: UIView!
    @IBOutlet weak var gasBackGround: UIView!
    @IBOutlet weak var motionBackGround: UIView!
    
    @IBOutlet weak var fireImage: UIImageView!
    @IBOutlet weak var gasImage: UIImageView!
    @IBOutlet weak var motionImage: UIImageView!
    
    @IBOutlet weak var fireLabel: UILabel!
    @IBOutlet weak var gasLabel: UILabel!
    @IBOutlet weak var motionLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        creatNav()
        settingView1.isHidden = true
        closedState1.isHidden = false
        settingView2.isHidden = true
        closedState2.isHidden = true
        settingView3.isHidden = true
        closedState3.isHidden = true
        
    }
    
    @IBAction func fireBtnClicked(_ sender: Any) {
        
        index = 1
        fire = !fire
        gas = false
        motion = false
        setIconChange()
        
        
    }
    
    @IBAction func gasBtnClicked(_ sender: Any) {
        
        index = 2
        fire = false
        gas = !gas
        motion = false
        
        setIconChange()
        
    }
    @IBAction func motionBtnClicked(_ sender: Any) {
        
        index = 3
        fire = false
        gas = false
        motion = !motion
        setIconChange()
    }
    
    func settingChange(_ index : Int, option : Bool) {
        switch index {
        case 1:
             if option {
                settingView1.isHidden = false
                closedState1.isHidden = true
                settingView2.isHidden = true
                closedState2.isHidden = true
                settingView3.isHidden = true
                closedState3.isHidden = true
                break
                
             }
             else {
                settingView1.isHidden = true
                closedState1.isHidden = false
                settingView2.isHidden = true
                closedState2.isHidden = true
                settingView3.isHidden = true
                closedState3.isHidden = true
                break
            }
        case 2:
            if option {
               settingView2.isHidden = false
               closedState2.isHidden = true

                settingView1.isHidden = true
                closedState1.isHidden = true
                settingView3.isHidden = true
                closedState3.isHidden = true
                break

            }
            else {
               settingView2.isHidden = true
               closedState2.isHidden = false
                settingView1.isHidden = true
                closedState1.isHidden = true
                settingView3.isHidden = true
                closedState3.isHidden = true
                break
           }
        case 3:
            if option {
               settingView3.isHidden = false
               closedState3.isHidden = true

                settingView1.isHidden = true
                closedState1.isHidden = true
                settingView2.isHidden = true
                closedState2.isHidden = true
                break

            }
            else {
                settingView3.isHidden = true
                closedState3.isHidden = false
                settingView1.isHidden = true
                closedState1.isHidden = true
                settingView2.isHidden = true
                closedState2.isHidden = true
                break
           }

            
        default:
            print("default")
        }
       
    }
    
    func setIconChange() {
        
        if index == 1 {
            settingChange(index,option: fire)
            
        }
        
        if index == 2 {
            settingChange(index,option: gas)
            
        }
        
        if index == 3 {
            settingChange(index,option: motion)
            
        }
        
        if fire {
           
            fireBackGround.backgroundColor = UIColor.init(named: "colorback")
            fireImage.image = UIImage.init(named: "fire_SELECTED")
            fireLabel.textColor = UIColor.black
 
        }
        if !fire{
            
            fireBackGround.backgroundColor = UIColor.init(named: "colorTopbar")
            fireImage.image = UIImage.init(named: "fire")
            fireLabel.textColor = UIColor.white
            
        }
        
        if gas {
            
            gasBackGround.backgroundColor = UIColor.init(named: "colorback")
            gasImage.image = UIImage.init(named: "gas_SELECTED")
            gasLabel.textColor = UIColor.black
  
        }
        
        if !gas {
            
            gasBackGround.backgroundColor = UIColor.init(named: "colorTopbar")
            gasImage.image = UIImage.init(named: "gas")
            gasLabel.textColor = UIColor.white
            
        }
        
        if motion {
            
            motionBackGround.backgroundColor = UIColor.init(named: "colorback")
            motionImage.image = UIImage.init(named: "motion_SELECTED")
            motionLabel.textColor = UIColor.black
            
        }
        if !motion {
            
            motionBackGround.backgroundColor = UIColor.init(named: "colorTopbar")
            motionImage.image = UIImage.init(named: "motion")
            motionLabel.textColor = UIColor.white

        }
    }
    
}
