//
//  ViewController.swift
//  DPTimePicker
//
//  Created by Dario Pellegrini on 07/10/16.
//  Copyright © 2016 Dario Pellegrini. All rights reserved.
//

import UIKit

extension TimePickerDialog : DPTimePickerDelegate {
    
    func timePickerDidConfirm(_ hour: String, minute: String, timePicker: DPTimePicker) {
        let alert = UIAlertController(title: "Time selected", message: "\(hour) : \(minute)", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func timePickerDidClose(_ timePicker: DPTimePicker) {
        let alert = UIAlertController(title: "Closed", message: "Time picker closed", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}

class TimePickerDialog: UIViewController {
    
    var fromam = false
    var frompm = false
    var toam = false
    var topm = false
    
    var s = false
    var m = false
    var t = false
    var w = false
    var th = false
    var f = false
    var st = false
    
    @IBOutlet weak var topview: UIView!
    @IBOutlet weak var bottomview: UIView!
    
    @IBOutlet weak var fromamview: UIView!
    @IBOutlet weak var frompmview: UIView!
    @IBOutlet weak var toamview: UIView!
    @IBOutlet weak var topmview: UIView!
    
    @IBOutlet weak var sview: UIView!
    @IBOutlet weak var mview: UIView!
    @IBOutlet weak var tview: UIView!
    @IBOutlet weak var wview: UIView!
    @IBOutlet weak var thview: UIView!
    @IBOutlet weak var fview: UIView!
    @IBOutlet weak var stview: UIView!
    
    @IBOutlet weak var sLabel: UILabel!
    @IBOutlet weak var mLabel: UILabel!
    @IBOutlet weak var tLabel: UILabel!
    @IBOutlet weak var wLabel: UILabel!
    @IBOutlet weak var thLabel: UILabel!
    @IBOutlet weak var fLabel: UILabel!
    @IBOutlet weak var stLabel: UILabel!
    
   
    
    @IBOutlet weak var fam: UILabel!
    @IBOutlet weak var fpm: UILabel!
    @IBOutlet weak var tam: UILabel!
    @IBOutlet weak var tpm: UILabel!
    
    let timePicker: DPTimePicker = DPTimePicker.timePicker()
    let timePicker1: DPTimePicker = DPTimePicker.timePicker()
    //let redColor = UIColor.red
    override func viewDidLoad() {
        super.viewDidLoad()
        UIApplication.shared.statusBarStyle = .lightContent
        
        timePicker.insertInView(topview)
        timePicker.delegate = self
        timePicker.closeButton.setTitleColor(UIColor.black, for: .normal)
        timePicker.closeButton.setTitle("Close", for: .normal)
        timePicker.okButton.setTitleColor(UIColor.black, for: .normal)
        timePicker.okButton.setTitle("OK", for: .normal)
        timePicker.backgroundColor = UIColor.white
        timePicker.numbersColor = UIColor.black
        timePicker.linesColor = UIColor.gray
        timePicker.pointsColor = UIColor.white
        timePicker.topGradientColor = UIColor.white
        timePicker.bottomGradientColor = UIColor.white
        timePicker.fadeAnimation = true
        timePicker.springAnimations = true
        timePicker.scrollAnimations = true
        timePicker.areLinesHidden = false
        timePicker.arePointsHidden = false
        timePicker.initialHour = "15"
        timePicker.initialMinute = "12"
        
        timePicker1.insertInView(bottomview)
        timePicker1.delegate = self
        timePicker1.closeButton.setTitleColor(UIColor.black, for: .normal)
        timePicker1.closeButton.setTitle("Close", for: .normal)
        timePicker1.okButton.setTitleColor(UIColor.black, for: .normal)
        timePicker1.okButton.setTitle("OK", for: .normal)
        timePicker1.backgroundColor = UIColor.white
        timePicker1.numbersColor = UIColor.black
        timePicker1.linesColor = UIColor.gray
        timePicker1.pointsColor = UIColor.white
        timePicker1.topGradientColor = UIColor.white
        timePicker1.bottomGradientColor = UIColor.white
        timePicker1.fadeAnimation = true
        timePicker1.springAnimations = true
        timePicker1.scrollAnimations = true
        timePicker1.areLinesHidden = false
        timePicker1.arePointsHidden = false
        timePicker1.initialHour = "15"
        timePicker1.initialMinute = "12"
        
        
        timePicker.show(nil)
        timePicker1.show(nil)
    }
    
    func setState(_ index : Int)  {
        
        switch index {
        case 1:
            if fromam {
                fromamview.backgroundColor = UIColor.black
                fam.textColor = UIColor.white
                
                frompmview.backgroundColor = UIColor.white
                fpm.textColor = UIColor.black
                break
            }
            
            else if frompm {
                fromamview.backgroundColor = UIColor.white
                fam.textColor = UIColor.black
                
                frompmview.backgroundColor = UIColor.black
                fpm.textColor = UIColor.white
                break
            }
        case 2 :
            if toam {
                toamview.backgroundColor = UIColor.black
                tam.textColor = UIColor.white
                
                topmview.backgroundColor = UIColor.white
                tpm.textColor = UIColor.black
                break
                    }
                    
            else if topm {
                toamview.backgroundColor = UIColor.white
                tam.textColor = UIColor.black
                
                topmview.backgroundColor = UIColor.black
                tpm.textColor = UIColor.white
                break
            }
        default:
            print("default")
        }
        
        
        
    }
    @IBAction func fromAm(_ sender: UIButton) {
          fromam = !fromam
          frompm = !fromam
        
          setState(1)
    }
    @IBAction func fromPm(_ sender: UIButton) {
        frompm = !frompm
        fromam = !frompm
        setState(1)
        
    }
    @IBAction func toAm(_ sender: UIButton) {
        toam = !toam
        topm = !toam
        setState(2)
    }
    @IBAction func toPm(_ sender: UIButton) {
        topm = !topm
        toam = !topm
        setState(2)
    }
    
    @IBAction func s(_ sender: UIButton) {
        s = !s
        setDayChange(1)
    }
    @IBAction func m(_ sender: UIButton) {
        m = !m
        setDayChange(2)
        
    }
    @IBAction func t(_ sender: UIButton) {
        t = !t
        setDayChange(3)
        
    }
    @IBAction func w(_ sender: UIButton) {
        w  = !w
        setDayChange(4)
        
    }
    @IBAction func th(_ sender: UIButton) {
        th = !th
        setDayChange(5)
        
    }
    @IBAction func f(_ sender: UIButton) {
        f = !f
        setDayChange(6)
        
    }
    @IBAction func st(_ sender: UIButton) {
        st = !st
        setDayChange(7)
        
    }
    func setDayChange(_ index : Int) {
        switch index {
        case 1:
            if s {
                
                sview.backgroundColor = UIColor.black
                sLabel.textColor = UIColor.white
            }
            else{
                sview.backgroundColor = UIColor.white
                sLabel.textColor = UIColor.black
            }
            case 2:
            if m {
                
                mview.backgroundColor = UIColor.black
                mLabel.textColor = UIColor.white
            }
            else{
                mview.backgroundColor = UIColor.white
                mLabel.textColor = UIColor.black
            }
            case 3:
            if t {
                
                tview.backgroundColor = UIColor.black
                tLabel.textColor = UIColor.white
            }
            else{
                tview.backgroundColor = UIColor.white
                tLabel.textColor = UIColor.black
            }
            case 4:
            if w {
                
                wview.backgroundColor = UIColor.black
                wLabel.textColor = UIColor.white
            }
            else{
                wview.backgroundColor = UIColor.white
                wLabel.textColor = UIColor.black
            }
            case 5:
            if th {
                
                thview.backgroundColor = UIColor.black
                thLabel.textColor = UIColor.white
            }
            else{
                thview.backgroundColor = UIColor.white
                thLabel.textColor = UIColor.black
            }
            case 6:
            if f {
                
                fview.backgroundColor = UIColor.black
                fLabel.textColor = UIColor.white
            }
            else{
                fview.backgroundColor = UIColor.white
                fLabel.textColor = UIColor.black
            }
            case 7:
            if st {
                
                stview.backgroundColor = UIColor.black
                stLabel.textColor = UIColor.white
            }
            else{
                stview.backgroundColor = UIColor.white
                stLabel.textColor = UIColor.black
            }
        default:
            print("default")
        }
        
        
    }
   
    @IBAction func gotoBack(_ sender: Any) {
         self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func closeBtnClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}




