//
//  SplashVC.swift
//  Vervet
//
//  Created by Admin on 11/10/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class SplashVC: BaseVC1 {

    override func viewDidLoad() {
        super.viewDidLoad()

        gotoLoginNav()
    }
    
    func gotoLoginNav()  {
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
                loginNav.modalPresentationStyle = .fullScreen
                     self.present(loginNav, animated: true, completion: nil)
                        
                }
            )
        }
}
