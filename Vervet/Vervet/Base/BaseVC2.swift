//
//  BaseVC3.swift
//  Vervet
//
//  Created by Ubuntu on 11/18/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import Foundation
import UIKit

class BaseVC2: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    func creatNav() {

        filterMenuVC = (UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "FilterMenuVC") as! FilterMenuVC)
        filterMenuVC.view.frame = CGRect(x: UIScreen.main.bounds.width * 2, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)

        lightVC = (UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LightVC") as! LightVC)
        lightVC.view.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
    }
    func openMenu(){

        lightVC.view.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        self.view.addSubview(lightVC.view)
        self.addChild(lightVC)
        UIView.animate(withDuration: 0.3, animations: {
            filterMenuVC.view.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
            self.view.addSubview(filterMenuVC.view)
            self.addChild(filterMenuVC)
        })
    }
    
    func closeMenu(){

        lightVC.view.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        lightVC.view.removeFromSuperview()
        UIView.animate(withDuration: 0.3, animations: {
            filterMenuVC.view.frame = CGRect(x: UIScreen.main.bounds.width * 2, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
            //self.menuVC.view.removeFromSuperview()
            filterMenuVC.view.willRemoveSubview(menuVC.view)
        })
    }
    
    func gotoNavPresent(_ storyname : String) {

            let toVC = self.storyboard?.instantiateViewController(withIdentifier: storyname)
            toVC?.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(toVC!, animated: true)

    }
}
