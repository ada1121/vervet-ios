//
//  AppDelegate.swift
//  Vervet
//
//  Created by Admin on 11/9/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit
import CoreData
import HHTabBarView
import IQKeyboardManagerSwift
import Firebase
import GoogleSignIn
import FBSDKCoreKit

let FB_SCHEME = "fb413330522908020"
let GOOGLE_SCHEME  = "com.googleusercontent.apps.979436194844-gpopjunu2jp1mroqd7cd9nl5j9kttfu5"

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    /////Start ------ Setup HHTabBarView ------
    
    //1
    //Initialize and keeping reference of HHTabBarView.
    public static var hhTabBarView = HHTabBarView.shared
    
    //Keeping reference of iOS default UITabBarController.
    public static let referenceUITabBarController = HHTabBarView.shared.referenceUITabBarController
    
    //2
    public static func setupReferenceUITabBarController() {
        
        //Creating a storyboard reference
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        
        //First and Second Tab ViewControllers will be taken from the UIStoryBoard
        //Creating navigation controller for navigation inside the first tab.
        let navigationController1: UINavigationController = UINavigationController(rootViewController: storyboard.instantiateViewController(withIdentifier: "FireTabVC"))
        navigationController1.isNavigationBarHidden = true
        
        let navigationController2: UINavigationController = UINavigationController(rootViewController: storyboard.instantiateViewController(withIdentifier: "DeviceVC"))
        navigationController2.isNavigationBarHidden = true
        
        let navigationController3: UINavigationController = UINavigationController(rootViewController: storyboard.instantiateViewController(withIdentifier: "NotiVC"))
        navigationController3.isNavigationBarHidden = true
        
        let navigationController4: UINavigationController = UINavigationController(rootViewController: storyboard.instantiateViewController(withIdentifier: "PresetsVC"))
        navigationController4.isNavigationBarHidden = true
        
      
        
        //Update referenced TabbarController with your viewcontrollers
        AppDelegate.referenceUITabBarController.setViewControllers([navigationController1, navigationController2,navigationController3, navigationController4 ], animated: false)
    }
    
    //3
    public static func setupHHTabBarView() {
        
        //Default & Selected Background Color
        let defaultTabColor = UIColor.white.withAlphaComponent(0.8)
        let selectedTabColor = UIColor(red: 232/255, green: 232/255, blue: 232/255, alpha:0.0)
        let tabFont = UIFont.init(name: "Helvetica-Light", size: 12.0)
        let spacing: CGFloat = 3.0
        
        //Create Custom Tabs
        //Note: As tabs are subclassed of UIButton so you can modify it as much as possible.
        
//        let titles = ["Location", "Refresh", "Sofa", "Target", "Umbrella"]
        let icons = [UIImage(named: "home_UNSELECTED")!, UIImage(named: "device_settings_UNSELECTED")!, UIImage(named: "notifications_UNSELECTED")!, UIImage(named: "presets_UNSELECTED")!]
        
        let icons_selected = [UIImage(named: "HOME_SELECTED")!, UIImage(named: "device_SELECTED")!, UIImage(named: "notifications_SELECTED")!, UIImage(named: "presets_SELECTED")!]
        
        
        var tabs = [HHTabButton]()
        
        for index in 0...3 {
            let tab = HHTabButton(withTitle: nil, tabImage: icons[index], index: index)
            tab.setImage(icons_selected[index], for: .selected)
            
            tab.titleLabel?.font = tabFont
            tab.setHHTabBackgroundColor(color: defaultTabColor, forState: .normal)
            tab.setHHTabBackgroundColor(color: selectedTabColor, forState: .selected)
            tab.imageToTitleSpacing = spacing
            tab.imageVerticalAlignment = .top
            tab.imageHorizontalAlignment = .center
            
            tabs.append(tab)
        }
        
        //Set HHTabBarView position.
        hhTabBarView.tabBarViewPosition = .bottom
        
        //Set this value according to your UI requirements.
        hhTabBarView.tabBarViewTopPositionValue = 128

        //Set Default Index for HHTabBarView.
        hhTabBarView.tabBarTabs = tabs
        
        
        // To modify badge label.
        // Note: You should only modify badgeLabel after assigning tabs array.
        // Example:
        //t1.badgeLabel?.backgroundColor = .white
        //t1.badgeLabel?.textColor = selectedTabColor
        //t1.badgeLabel?.font = UIFont.boldSystemFont(ofSize: 20.0)
        
        //Handle Tab Change Event
        hhTabBarView.defaultIndex = 0
        
        //Show Animation on Switching Tabs
        hhTabBarView.tabChangeAnimationType = .shake
        
        //Handle Tab Changes
        hhTabBarView.onTabTapped = { (tabIndex, isSameTab, controller) in
            if isSameTab {
                if let navcon = controller as? UINavigationController {
                    navcon.popToRootViewController(animated: true)
                } else if let vc = controller as? UIViewController {
                    vc.navigationController?.popToRootViewController(animated: true)
                }
            }
            print("Selected Tab Index:\(tabIndex)")
            
            
        }
    }

    var loginVC : LoginVC!
    

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        FirebaseApp.configure()
        // for google signin
        GIDSignIn.sharedInstance()?.clientID = FirebaseApp.app()?.options.clientID
        
        
        
        ApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        loginVC = (storyBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC)
        loginNav = NavigationController(rootViewController: loginVC)
        
        IQKeyboardManager.shared.enable = true
        return true
    }

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        
        if url.scheme == FB_SCHEME {
            return ApplicationDelegate.shared.application(
                application,
                open: url,
                sourceApplication: sourceApplication,
                annotation: annotation
            )
        } else {
            
            return (GIDSignIn.sharedInstance()?.handle(url))!
        }
        
    }
    
    
    @available(iOS 9.0, *)
    func application(_ application: UIApplication,
                     open url: URL,
                     options: [UIApplication.OpenURLOptionsKey: Any]) -> Bool {
        
        if url.scheme == FB_SCHEME {
            return ApplicationDelegate.shared.application(application, open: url, options: options)
        } else {
            
            return (GIDSignIn.sharedInstance()?.handle(url))!
        }
    }

    // MARK: - Core Data stack

    lazy var persistentContainer: NSPersistentCloudKitContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentCloudKitContainer(name: "Vervet")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }

}

class NavigationController: UINavigationController {

    //---------------------------------------------------------------------------------------------------------------------------------------------
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationBar.isTranslucent = false
        navigationBar.barTintColor = UIColor(red:0.00, green:0.20, blue:0.40, alpha:1.0)
        navigationBar.tintColor = UIColor.white
        navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
    }

    //---------------------------------------------------------------------------------------------------------------------------------------------
    override var preferredStatusBarStyle: UIStatusBarStyle {

        return .lightContent
    }
}

